package com.anosym.cassandra.annotation;

import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Table;
import org.atteo.classindex.processor.ClassIndexProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 9:08:00 PM
 */
public class AccessorAnnotationProcessor extends ClassIndexProcessor {

    public AccessorAnnotationProcessor() {
        indexAnnotations(Accessor.class, Table.class);
    }

}
