package com.anosym.cassandra.repository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.Extension;

import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Table;
import com.google.common.collect.ImmutableSet;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.atteo.classindex.ClassIndex;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 5:25:42 PM
 */
@Slf4j
public class RepositoryExtension implements Extension {

    private static final Map<Class<?>, Class<?>> REPOSITORY_CLASSES;

    private static final Set<Class<?>> DOMAIN_CLASSES;

    static {
        REPOSITORY_CLASSES = ImmutableSet
                .copyOf(ClassIndex.getAnnotated(Accessor.class))
                .stream()
                .filter(Class::isInterface)
                .peek((repositoryClass) -> log.info("--- Found <{}> ", repositoryClass.getCanonicalName()))
                .collect(toMap(RepositoryExtension::getDomainClassOrDefault, Function.identity()));
        DOMAIN_CLASSES = ImmutableSet
                .copyOf(ClassIndex.getAnnotated(Table.class))
                .stream()
                .filter(RepositoryExtension::hasNoAccessorBean)
                .peek((domainClass) -> log.info("--- Found <{}> ", domainClass.getCanonicalName()))
                .collect(toSet());
    }

    void afterBeanDiscovery(@Observes final AfterBeanDiscovery afterBeanDiscovery) {
        log.info("Registering accessor repository beans for: {}", REPOSITORY_CLASSES);

        REPOSITORY_CLASSES
                .values()
                .stream()
                .map(RepositoryBean::new)
                .forEach(afterBeanDiscovery::addBean);

        //registering additional beans
        log.debug("Registering crud-repository beans for: {}", DOMAIN_CLASSES);

        DOMAIN_CLASSES
                .stream()
                .map(CrudRepositoryBean::new)
                .forEach(afterBeanDiscovery::addBean);
    }

    private static boolean hasNoAccessorBean(@NonNull final Class<?> domainType) {
        return !REPOSITORY_CLASSES.containsKey(domainType);
    }

    @NonNull
    private static Class<?> getDomainClassOrDefault(@NonNull final Class<?> accessorInterface) {
        final Type[] genericInterfaces = accessorInterface.getGenericInterfaces();
        if (genericInterfaces.length == 0) {
            return accessorInterface;
        }

        if (!(genericInterfaces[0] instanceof ParameterizedType)) {
            return accessorInterface;
        }

        return (Class<?>) ((ParameterizedType) genericInterfaces[0]).getActualTypeArguments()[0];
    }

}
