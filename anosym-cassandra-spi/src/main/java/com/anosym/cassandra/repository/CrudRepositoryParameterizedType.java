package com.anosym.cassandra.repository;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

import com.anosym.cassandra.annotation.CompositeKeyClass;
import com.datastax.driver.mapping.annotations.PartitionKey;
import lombok.NonNull;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 6, 2018, 1:10:22 PM
 */
public class CrudRepositoryParameterizedType implements ParameterizedType {

    private final Type domainClass;

    private final Type partitionKeyClass;

    private final Type[] actualArguments;

    private CrudRepositoryParameterizedType(@NonNull final Type domainClass, @NonNull final Type partitionKeyClass) {
        this.domainClass = domainClass;
        this.partitionKeyClass = partitionKeyClass;
        this.actualArguments = new Type[]{domainClass, partitionKeyClass};
    }

    @NonNull
    public static ParameterizedType crudRepositoryParameterizedType(@NonNull final Class<?> domainType) {
        final Type partitionKeyClass = domainType.isAnnotationPresent(CompositeKeyClass.class)
                ? domainType.getAnnotation(CompositeKeyClass.class).value() : getFieldPartitionKeyType(domainType);
        return new CrudRepositoryParameterizedType(domainType, partitionKeyClass);
    }

    @Override
    public Type[] getActualTypeArguments() {
        return actualArguments;
    }

    @Override
    public Type getRawType() {
        return CrudRepository.class;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }

    @Override
    public String getTypeName() {
        return format("%s<%s, %s>",
                      CrudRepository.class.getName(),
                      domainClass.getTypeName(),
                      partitionKeyClass.getTypeName());
    }

    @Override
    public String toString() {
        return getTypeName();
    }

    @NonNull
    private static Type getFieldPartitionKeyType(@NonNull final Class<?> domainType) {
        return Arrays
                .stream(domainType.getDeclaredFields())
                .filter((field) -> field.isAnnotationPresent(PartitionKey.class))
                .map(Field::getType)
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }

}
