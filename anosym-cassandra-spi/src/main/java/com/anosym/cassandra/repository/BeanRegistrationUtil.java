package com.anosym.cassandra.repository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Set;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;
import lombok.NonNull;

import static java.lang.String.format;

import static com.google.common.base.Preconditions.checkArgument;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 6, 2018, 10:12:48 AM
 */
public class BeanRegistrationUtil {

    @Nonnull
    public static Set<Type> resolveBeanTypes(@Nonnull final Type beanInterface) {
        final ImmutableSet.Builder<Type> builder = ImmutableSet.builder();
        builder.add(beanInterface);

        final Class<?>[] interfaces = ((Class<?>) beanInterface).getInterfaces();
        if (interfaces != null && interfaces.length > 0) {
            builder.addAll(resolveBeanTypes(interfaces[0]));
        }

        return builder.build();
    }

    @NonNull
    public static String resolveBeanName(@NonNull final Class<?> beanClass) {
        final String simpleName = beanClass.getSimpleName();
        return format("%s%s", simpleName.substring(0, 1).toLowerCase(), simpleName.substring(1));
    }

    @NonNull
    public static <T> Class<T> resolveDomainType(@NonNull final Class<?> accessInterface) {
        final Type genericInterface = accessInterface.getGenericInterfaces()[0];
        checkArgument(genericInterface instanceof ParameterizedType,
                      "Invalid access bean. Must have type arguments: %s",
                      accessInterface);

        return (Class<T>) ((ParameterizedType) genericInterface).getActualTypeArguments()[0];
    }

}
