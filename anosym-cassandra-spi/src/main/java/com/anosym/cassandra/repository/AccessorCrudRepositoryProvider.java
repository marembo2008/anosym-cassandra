package com.anosym.cassandra.repository;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import lombok.NonNull;

import static java.util.stream.Collectors.toList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 7, 2018, 9:19:13 PM
 */
public class AccessorCrudRepositoryProvider<A, T, PK> implements InvocationHandler {

    private static final List<Method> CRUD_REPOSITYR_METHODS = Arrays
            .stream(CrudRepository.class.getDeclaredMethods())
            .collect(toList());

    private final CrudRepository<T, PK> domainCrudRepository;

    private final A domainAccessor;

    private final Class<?>[] beanClasses;

    public AccessorCrudRepositoryProvider(@NonNull final Session session,
                                          @NonNull final Class<A> accessInterface,
                                          @NonNull final Set<Class<?>> beanClasses,
                                          @NonNull final Class<T> domainType) {
        final MappingManager mappingManager = new MappingManager(session);
        this.domainAccessor = mappingManager.createAccessor(accessInterface);
        this.domainCrudRepository = new SimpleCrudRepository<>(session, domainType);
        this.beanClasses = beanClasses.stream().toArray(Class<?>[]::new);
    }

    public A provide() {
        return (A) Proxy.newProxyInstance(getClass().getClassLoader(), beanClasses, this);
    }

    @Override
    public Object invoke(@NonNull final Object proxy,
                         @NonNull final Method method,
                         @NonNull final Object[] args) throws Throwable {
        if (CRUD_REPOSITYR_METHODS.contains(method)) {
            return method.invoke(domainCrudRepository, args);
        }

        return method.invoke(domainAccessor, args);
    }

}
