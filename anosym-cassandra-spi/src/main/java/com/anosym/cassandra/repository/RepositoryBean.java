package com.anosym.cassandra.repository;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.CDI;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.PassivationCapable;
import javax.inject.Qualifier;

import com.anosym.cassandra.annotation.KeyspaceScoped;
import com.anosym.cdi.spiimpl.AnyAnnotation;
import com.anosym.cdi.spiimpl.DefaultAnnotation;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import com.google.common.collect.ImmutableSet;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import static com.anosym.cassandra.repository.BeanRegistrationUtil.resolveDomainType;
import static com.anosym.cassandra.repository.CrudRepositoryParameterizedType.crudRepositoryParameterizedType;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 5:29:33 PM
 */
@Slf4j
public final class RepositoryBean implements Bean<Object>, PassivationCapable {

    @Nonnull
    private final Class<?> repositoryInterface;

    @Nonnull
    private final Set<Class<?>> beanClasses;

    @Nonnull
    private final Set<Type> resolvedTypes;

    @Nonnull
    private final Set<Class<? extends Annotation>> stereotypes;

    @Nonnull
    private final String beanName;

    @NonNull
    private final Set<Annotation> qualifiers;

    private final boolean alternative;

    private final String id = UUID.randomUUID().toString();

    private final boolean crudRepository;

    private final Class<?> domainType;

    public RepositoryBean(@NonNull final Class<?> repositoryInterface) {
        this.repositoryInterface = repositoryInterface;
        this.beanClasses = resolveBeanClasses(repositoryInterface);
        this.stereotypes = resolveStereotypes(beanClasses);
        this.beanName = resolveBeanName(repositoryInterface);
        this.qualifiers = resolveQualifiers(beanClasses);
        this.alternative = resolveAlternative(repositoryInterface);
        this.crudRepository = beanClasses.stream().anyMatch(CrudRepository.class::isAssignableFrom);
        this.domainType = crudRepository ? resolveDomainType(repositoryInterface) : null;
        this.resolvedTypes = resolveTypes(domainType, beanClasses);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Class<?> getBeanClass() {
        return repositoryInterface;
    }

    @Override
    public Set<InjectionPoint> getInjectionPoints() {
        return ImmutableSet.of();
    }

    @Override
    public boolean isNullable() {
        return false;
    }

    @Override
    public Object create(@NonNull final CreationalContext<Object> creationalContext) {
        final Session session = CDI.current().select(Session.class).get();

        log.debug("Creating repository bean <{}> for keyspace: {}", repositoryInterface, session.getLoggedKeyspace());

        if (crudRepository) {
            return new AccessorCrudRepositoryProvider(session, repositoryInterface, beanClasses, domainType).provide();
        }

        final MappingManager mappingManager = new MappingManager(session);
        return mappingManager.createAccessor(repositoryInterface);
    }

    @Override
    public void destroy(@NonNull final Object instance, @NonNull final CreationalContext<Object> creationalContext) {
        creationalContext.release();
    }

    @Override
    public Set<Type> getTypes() {
        return resolvedTypes;
    }

    @Override
    public Set<Annotation> getQualifiers() {
        return qualifiers;
    }

    @Override
    public Class<? extends Annotation> getScope() {
        return KeyspaceScoped.class;
    }

    @Override
    public String getName() {
        return beanName;
    }

    @Override
    public Set<Class<? extends Annotation>> getStereotypes() {
        return stereotypes;
    }

    @Override
    public boolean isAlternative() {
        return alternative;
    }

    @Nonnull
    private Set<Class<?>> resolveBeanClasses(@Nonnull final Class<?> repositoryInterface) {
        final ImmutableSet.Builder<Class<?>> builder = ImmutableSet.builder();
        builder.add(repositoryInterface);

        final Class<?>[] interfaces = repositoryInterface.getInterfaces();
        if (interfaces != null && interfaces.length > 0) {
            builder.addAll(resolveBeanClasses(interfaces[0]));
        }

        return builder.build();
    }

    @NonNull
    private Set<Class<? extends Annotation>> resolveStereotypes(@NonNull final Set<Class<?>> repositoryBeans) {
        return repositoryBeans
                .stream()
                .flatMap((cls) -> Arrays.stream(cls.getAnnotations()).map(Annotation::annotationType))
                .filter((annotType) -> annotType.isAnnotationPresent(Stereotype.class))
                .collect(toSet());
    }

    @NonNull
    private String resolveBeanName(@NonNull final Class<?> repositoryInterface) {
        final String simpleName = repositoryInterface.getSimpleName();
        return format("%s%s", simpleName.substring(0, 1).toLowerCase(), simpleName.substring(1));
    }

    @NonNull
    private Set<Annotation> resolveQualifiers(@NonNull final Set<Class<?>> repositoryBeans) {
        final Set<Annotation> declaredQualifiers = repositoryBeans
                .stream()
                .flatMap((cls) -> getQualifiers(cls))
                .collect(toSet());
        final ImmutableSet.Builder<Annotation> qualifiersBuilder = ImmutableSet.<Annotation>builder()
                .addAll(declaredQualifiers)
                .add(new AnyAnnotation());
        if (declaredQualifiers.isEmpty()) {
            qualifiersBuilder.add(new DefaultAnnotation());
        }

        return qualifiersBuilder.build();
    }

    @NonNull
    private Stream<Annotation> getQualifiers(@NonNull final Class<?> beanClass) {
        return Arrays
                .stream(beanClass.getAnnotations())
                .filter((annot) -> annot.annotationType().isAnnotationPresent(Qualifier.class));
    }

    private boolean resolveAlternative(@NonNull final Class<?> beanClass) {
        return beanClass.isAnnotationPresent(Alternative.class);
    }

    @NonNull
    private Set<Type> resolveTypes(@Nullable final Class<?> domainType, @NonNull final Set<Class<?>> beanClasses) {
        final ImmutableSet.Builder<Type> builder = ImmutableSet.<Type>builder().add(Object.class);
        if (domainType != null) {
            builder.add(crudRepositoryParameterizedType(domainType));

            final List<Class<?>> withoutCrudRepos = beanClasses.stream()
                    .filter((cls) -> cls != CrudRepository.class)
                    .collect(toList());
            builder.addAll(withoutCrudRepos);
        } else {
            builder.addAll(beanClasses);
        }

        final Set<Type> types = builder.build();

        log.info("Type for <{}>: {}", repositoryInterface, types);

        return types;
    }

}
