package com.anosym.cassandra.repository;

import java.util.List;

import javax.annotation.Nullable;

import com.anosym.cassandra.domain.Page;
import com.datastax.driver.core.PagingState;
import lombok.NonNull;
import org.atteo.classindex.IndexSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 11:03:18 PM
 *
 * @param PK if PK is defined as @CompositeKey, the crud repository mapper will read its fields to find the primary keys
 * for the table. We do not support nested @CompositeKey
 */
@IndexSubclasses
public interface CrudRepository<T, PK> {

    @Nullable
    T find(@NonNull final PK key);

    @NonNull
    List<T> findAll();

    @NonNull
    Page<T> findAll(@NonNull final Integer pageSize, @Nullable final PagingState page);

    void save(@NonNull final T value);

    void save(@NonNull final List<T> values);

    void delete(@NonNull final T value);

}
