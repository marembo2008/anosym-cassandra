package com.anosym.cassandra.repository;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import com.anosym.cassandra.domain.Page;
import com.datastax.driver.core.ExecutionInfo;
import com.datastax.driver.core.PagingState;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.Result;
import lombok.NonNull;

import static java.util.stream.Collectors.toList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 8:55:20 AM
 */
public class PagingHelper {

    @NonNull
    public static <T> Page<T> getPage(@NonNull final Result<T> mappingResult,
                                      @NonNull final Mapper<T> domainMapper,
                                      final int pageSize) {
        final int availableWithoutFetching = mappingResult.getAvailableWithoutFetching();
        final List<T> result = IntStream.range(0, availableWithoutFetching)
                .mapToObj((ix) -> mappingResult.one())
                .collect(toList());
        final PagingState nextPage = mappingResult.getAllExecutionInfo()
                .stream()
                .map(ExecutionInfo::getPagingState)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
        return new Page<>(result, nextPage, pageSize);
    }

}
