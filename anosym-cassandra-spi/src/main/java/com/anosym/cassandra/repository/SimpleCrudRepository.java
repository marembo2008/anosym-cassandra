package com.anosym.cassandra.repository;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import com.anosym.cassandra.annotation.CompositeKey;
import com.anosym.cassandra.domain.Page;
import com.datastax.driver.core.PagingState;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import lombok.NonNull;

import static java.lang.String.format;

import static com.anosym.cassandra.repository.PagingHelper.getPage;
import static com.google.common.base.MoreObjects.firstNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 7, 2018, 9:21:07 PM
 */
class SimpleCrudRepository<T, PK> implements CrudRepository<T, PK> {

    @NonNull
    private final Mapper<T> domainMapper;

    public SimpleCrudRepository(@NonNull final Session session, @NonNull final Class<T> domainType) {
        this.domainMapper = new MappingManager(session).mapper(domainType);
    }

    @Override
    public T find(@NonNull final PK key) {
        return domainMapper.get(getKeys(key));
    }

    @NonNull
    @Override
    public List<T> findAll() {
        final MappingManager mappingManager = domainMapper.getManager();
        final String table = domainMapper.getTableMetadata().getName();
        final String cql = format("select * from %s", table);
        final ResultSet resultSet = mappingManager.getSession().execute(cql);
        final Result<T> mappingResult = domainMapper.map(resultSet);
        return mappingResult.all();
    }

    @NonNull
    @Override
    public Page<T> findAll(@NonNull final Integer pageSize, @Nullable final PagingState page) {
        final MappingManager mappingManager = domainMapper.getManager();
        final String table = domainMapper.getTableMetadata().getName();
        final String cql = format("select * from %s", table);
        final Statement selectAllStatement = new SimpleStatement(cql);
        selectAllStatement.setFetchSize(pageSize);
        if (page != null) {
            selectAllStatement.setPagingState(page);
        }

        final ResultSet resultSet = mappingManager.getSession().execute(selectAllStatement);
        final Result<T> mappingResult = domainMapper.map(resultSet);
        return getPage(mappingResult, domainMapper, pageSize);
    }

    @Override
    public void save(@NonNull final T value) {
        domainMapper.save(value);
    }

    @Override
    public void save(@NonNull final List<T> values) {
        values.forEach(domainMapper::save);
    }

    @Override
    public void delete(@NonNull final T value) {
        domainMapper.delete(value);
    }

    @NonNull
    private Object[] getKeys(@NonNull final PK key) {
        if (!key.getClass().isAnnotationPresent(CompositeKey.class)) {
            return new Object[]{key};
        }

        return Arrays.stream(key.getClass().getDeclaredFields())
                .map((field) -> getKeyValue(field, key))
                .toArray();
    }

    @NonNull
    private Object getKeyValue(@NonNull final Field field, @NonNull final PK key) {
        field.setAccessible(true);

        try {
            //null values with empty string
            return firstNonNull(field.get(key), "");
        } catch (final IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

}
