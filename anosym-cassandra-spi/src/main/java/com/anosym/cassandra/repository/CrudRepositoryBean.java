package com.anosym.cassandra.repository;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.CDI;
import javax.enterprise.inject.spi.InjectionPoint;

import com.anosym.cassandra.annotation.KeyspaceScoped;
import com.anosym.cdi.spiimpl.AnyAnnotation;
import com.anosym.cdi.spiimpl.DefaultAnnotation;
import com.datastax.driver.core.Session;
import com.google.common.collect.ImmutableSet;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;

import static com.anosym.cassandra.repository.BeanRegistrationUtil.resolveBeanName;
import static com.anosym.cassandra.repository.CrudRepositoryParameterizedType.crudRepositoryParameterizedType;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 5, 2018, 11:56:34 AM
 */
@Slf4j
public class CrudRepositoryBean<E, PK, T extends CrudRepository<E, PK>> implements Bean<T> {

    private final Class<?> repositoryClass;

    private final Set<Type> repositoryBeanClasses;

    private final String beanName;

    private final Class<E> domainType;

    public CrudRepositoryBean(@NonNull final Class<E> domainType) {
        this.repositoryClass = CrudRepository.class;
        this.repositoryBeanClasses = ImmutableSet.of(crudRepositoryParameterizedType(domainType));

        log.debug("repositoryBeanClasses: {}", repositoryBeanClasses);

        this.beanName = format("%s.%s", resolveBeanName(domainType), repositoryClass.getSimpleName());
        this.domainType = domainType;
    }

    @Override
    public Class<?> getBeanClass() {
        return repositoryClass;
    }

    @Override
    public Set<InjectionPoint> getInjectionPoints() {
        return ImmutableSet.of();
    }

    @Override
    public boolean isNullable() {
        return false;
    }

    @Override
    public T create(@NonNull final CreationalContext<T> creationalContext) {
        final Session session = CDI.current().select(Session.class).get();

        log.debug("Creating bean for domain-type <{}> for keyspace: {}", domainType, session.getLoggedKeyspace());

        final CrudRepository<E, PK> domainRepository = new SimpleCrudRepository<>(session, domainType);
        return (T) domainRepository;
    }

    @Override
    public void destroy(@NonNull final T instance, @NonNull final CreationalContext<T> creationalContext) {
        creationalContext.release();
    }

    @Override
    public Set<Type> getTypes() {
        return repositoryBeanClasses;
    }

    @Override
    public Set<Annotation> getQualifiers() {
        return ImmutableSet.of(new DefaultAnnotation(), new AnyAnnotation());
    }

    @Override
    public Class<? extends Annotation> getScope() {
        return KeyspaceScoped.class;
    }

    @Override
    public String getName() {
        return beanName;
    }

    @Override
    public Set<Class<? extends Annotation>> getStereotypes() {
        return ImmutableSet.of();
    }

    @Override
    public boolean isAlternative() {
        return false;
    }

}
