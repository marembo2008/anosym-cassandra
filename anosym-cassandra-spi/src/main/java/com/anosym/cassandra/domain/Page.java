package com.anosym.cassandra.domain;

import java.util.List;

import javax.annotation.Nullable;

import com.datastax.driver.core.PagingState;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 10, 2018, 7:51:22 AM
 */
@Data
public class Page<T> {

    @NonNull
    private final List<T> contents;

    @Nullable
    private final PagingState pagingState;

    private final int pageSize;

}
