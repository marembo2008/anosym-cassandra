package com.anosym.cassandra.service;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;

/**
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:50:01 AM
 */
@Accessor
public interface KeyspaceAccessor {

    @Query("select keyspace_name, durable_writes from system_schema.keyspaces")
    Result<Keyspace> getAllKeyspaces();

}
