package com.anosym.cassandra.service;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:46:26 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "keyspaces", keyspace = "system_schema")
public class Keyspace {

    @PartitionKey
    @Column(name = "keyspace_name")
    private String name;

    @Column(name = "durable_writes")
    private boolean durableWrites;

}
