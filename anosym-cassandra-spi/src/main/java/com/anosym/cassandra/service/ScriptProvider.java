package com.anosym.cassandra.service;

import java.util.List;

import lombok.NonNull;

/**
 * Provides a list of scripts to execute upon keyspace registration.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:14:33 PM
 */
public interface ScriptProvider {

    @NonNull
    List<String> getScriptPaths(@NonNull final String keyspace);

}
