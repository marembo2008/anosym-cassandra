package com.anosym.cassandra.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.anosym.cassandra.annotation.Application;
import com.anosym.cassandra.config.ClusterConfigurationService;
import com.anosym.cassandra.config.KeyspaceConfigurationService;
import com.anosym.cassandra.config.StrategyClass;
import com.anosym.cdi.executionscoped.ExecutionPath;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import com.google.common.collect.ImmutableList;
import lombok.NonNull;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:10:05 AM
 */
@ApplicationScoped
public class KeyspaceDefinitionService {

    private static final String SIMPLE_STRATEGY_CQL = ""
            + "CREATE  KEYSPACE IF NOT EXISTS %s "
            + "  WITH REPLICATION = {"
            + "   'class': 'SimpleStrategy',"
            + "   'replication_factor': %s"
            + "  } "
            + "  AND DURABLE_WRITES = %s";

    private static final String NETWORK_TOPOLOGY_STRATEGY_CQL = ""
            + "CREATE  KEYSPACE IF NOT EXISTS %s "
            + "  WITH REPLICATION = {"
            + "   'class': 'NetworkTopologyStrategy',"
            + "   %s"
            + "  } "
            + "  AND DURABLE_WRITES = %s";

    @Inject
    @Application
    private Session applicationSession;

    @Inject
    private Instance<KeyspaceConfigurationService> keyspaceConfigurationService;

    @Inject
    private ClusterConfigurationService clusterConfigurationService;

    @Inject
    private KeyspaceScriptExecutor keyspaceScriptExecutor;

    @ExecutionPath
    public void addKeyspace(@NonNull final String keyspace) {
        final StrategyClass strategyClass = keyspaceConfigurationService.isUnsatisfied()
                ? StrategyClass.SimpleStrategy : keyspaceConfigurationService.get().getStrategyClass();
        final boolean durableWrites = keyspaceConfigurationService.isUnsatisfied()
                ? true : keyspaceConfigurationService.get().isDurableWritesEnabled();
        switch (strategyClass) {
            case NetworkTopologyStrategy:
                createKeyspaceWithNetworkTopologyStrategy(keyspace, durableWrites);
                break;
            case SimpleStrategy:
            default:
                createKeyspaceWithSimpleStrategy(keyspace, durableWrites);
        }

        // Execute keyspace scripts
        keyspaceScriptExecutor.onKeyspaceRegistered(keyspace);
    }

    /**
     * Non-system keyspaces
     */
    @NonNull
    public List<Keyspace> getApplicationKeyspaces() {
        final KeyspaceAccessor keyspaceAccessor = new MappingManager(applicationSession)
                .createAccessor(KeyspaceAccessor.class);
        final Result<Keyspace> keyspaces = keyspaceAccessor.getAllKeyspaces();
        return keyspaces.all();
    }

    private void createKeyspaceWithSimpleStrategy(@NonNull final String keyspace, final boolean durableWrites) {
        final int replicationFactor = keyspaceConfigurationService.isUnsatisfied()
                ? 1 : keyspaceConfigurationService.get().getSimpleStrategyReplicationFactor();
        final String cql = format(SIMPLE_STRATEGY_CQL, keyspace, replicationFactor, durableWrites);
        applicationSession.execute(cql);
    }

    private void createKeyspaceWithNetworkTopologyStrategy(@NonNull final String keyspace, final boolean durableWrites) {
        final List<String> clusters = keyspaceConfigurationService.isUnsatisfied()
                ? ImmutableList.of(clusterConfigurationService.getClusterName())
                : keyspaceConfigurationService.get().getClusters();
        final String replicationFactorsConfig = clusters
                .stream()
                .map(this::clusterReplicationFactor)
                .collect(joining(","));
        final String cql = format(NETWORK_TOPOLOGY_STRATEGY_CQL, keyspace, replicationFactorsConfig, durableWrites);
        applicationSession.execute(cql);
    }

    @NonNull
    private String clusterReplicationFactor(@NonNull final String clusterName) {
        final int replicationFactor = keyspaceConfigurationService.isUnsatisfied()
                ? 1 : keyspaceConfigurationService.get().getDataCentreReplicationFactor(clusterName);
        return format("'%s': %s", clusterName, replicationFactor);
    }

}
