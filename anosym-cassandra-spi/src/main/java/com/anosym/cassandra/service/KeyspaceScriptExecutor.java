package com.anosym.cassandra.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.anosym.cassandra.annotation.Application;
import com.anosym.cdi.executionscoped.ExecutionScoped;
import com.datastax.driver.core.Session;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:16:27 PM
 */
@Slf4j
@ExecutionScoped
public class KeyspaceScriptExecutor {

    private static final Splitter SEMI_COLLON = Splitter.on(';').trimResults().omitEmptyStrings();

    @Any
    @Inject
    private Instance<ScriptProvider> scriptProviders;

    @Inject
    @Application
    private Session session;

    public void onKeyspaceRegistered(@NonNull final String keyspace) {
        log.info("Preparing scripts to execute on keyspace");

        final List<String> scriptSources = ImmutableList.copyOf(scriptProviders)
                .stream()
                .flatMap((provider) -> provider.getScriptPaths(keyspace).stream())
                .collect(toList());

        log.info("Executing scripts for keyspace <{}>: {}", keyspace, scriptSources);

        session.execute("USE " + keyspace);

        scriptSources.forEach(this::executeScript);
    }

    @SuppressWarnings("UseSpecificCatch")
    private void executeScript(@NonNull final String scriptSource) {
        try {
            final Path cqlPath = Paths.get(getClass().getResource(scriptSource).toURI());
            final String cqlScript = Files.lines(cqlPath)
                    .collect(joining(" "));

            SEMI_COLLON.splitToList(cqlScript)
                    .stream()
                    .filter((script) -> !script.isEmpty())
                    .map(this::sanitizeScript)
                    .peek((script) -> log.info("Executing script <{}>", script))
                    .forEach(session::execute);

        } catch (final Exception ex) {
            log.warn("Failed to execute script <{}>", scriptSource, ex);
        }
    }

    @NonNull
    private String sanitizeScript(@NonNull final String script) {
        final int ix = script.lastIndexOf(";");
        if (ix == script.length() - 1) {
            return script.substring(0, script.length() - 1);
        }

        return script;
    }

}
