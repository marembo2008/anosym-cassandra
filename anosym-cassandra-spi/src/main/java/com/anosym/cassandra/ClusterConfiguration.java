package com.anosym.cassandra;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.anosym.cassandra.annotation.Application;
import com.anosym.cassandra.annotation.KeyspaceScoped;
import com.anosym.cassandra.keyspace.KeyspaceProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 4:44:18 PM
 */
@Slf4j
@ApplicationScoped
public class ClusterConfiguration {

    @Nonnull
    private final ClusterInitializer clusterInitializers;

    @Nonnull
    private final KeyspaceProvider keyspaceProvider;

    @Inject
    public ClusterConfiguration(@NonNull final ClusterInitializer clusterInitializers,
                                @NonNull final KeyspaceProvider keyspaceProvider) {
        this.clusterInitializers = clusterInitializers;
        this.keyspaceProvider = keyspaceProvider;
    }

    @Produces
    @ApplicationScoped
    public Cluster initCluster() {
        return Cluster.buildFrom(clusterInitializers);
    }

    @Default
    @Produces
    @KeyspaceScoped
    public Session initSession(final Cluster cluster) {
        final Session session = cluster.connect();
        final ResultSet releaseVersion = session.execute("select release_version from system.local");
        final Row row = releaseVersion.one();

        log.debug("Connected to Cassandra version: {}", row.toString());

        final String keyspace = keyspaceProvider.getKeyspace();
        checkState(keyspace != null, "Cannot create a session without keyspace");

        log.debug("using keyspace: {}", keyspace);

        session.execute("USE " + keyspace);

        log.info("Current session keyspace: {}", session.getLoggedKeyspace());

        return session;
    }

    @Produces
    @Dependent
    @Application
    public Session initApplicationSession(final Cluster cluster) {
        final Session session = cluster.connect();
        final ResultSet releaseVersion = session.execute("select release_version from system.local");
        final Row row = releaseVersion.one();

        log.debug("Connected to Cassandra version: {}", row.toString());

        return session;
    }

    public void closeCluster(@Disposes final Cluster cluster) {
        cluster.closeAsync();
    }

    public void closeSession(@Disposes final Session session) {
        session.closeAsync();
    }

    public void closeApplicationSession(@Disposes @Application final Session session) {
        session.closeAsync();
    }

}
