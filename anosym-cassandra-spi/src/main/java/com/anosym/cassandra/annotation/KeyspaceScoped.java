package com.anosym.cassandra.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.context.NormalScope;

/**
 * To be used for sessions scoped to a keyspace.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 11:25:14 PM
 */
@NormalScope
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface KeyspaceScoped {
}
