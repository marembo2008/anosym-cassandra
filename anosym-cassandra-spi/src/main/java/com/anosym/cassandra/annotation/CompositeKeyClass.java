package com.anosym.cassandra.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 7:46:57 AM
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CompositeKeyClass {

    /**
     * The class that is used as the composite key for the entity.
     */
    Class<?> value();

}
