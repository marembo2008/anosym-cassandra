package com.anosym.cassandra.config;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:12:56 AM
 */
@RequiredArgsConstructor
public enum StrategyClass {

    SimpleStrategy("SimpleStrategy"),
    NetworkTopologyStrategy("NetworkTopologyStrategy");

    @Getter
    @NonNull
    private final String classCode;

}
