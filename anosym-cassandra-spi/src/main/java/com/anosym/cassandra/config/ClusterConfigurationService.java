package com.anosym.cassandra.config;

import java.net.InetSocketAddress;
import java.util.List;

import javax.annotation.Nonnull;

import com.anosym.khameleon.core.annotations.Default;
import com.anosym.khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 12:19:00 PM
 */
public interface ClusterConfigurationService {

    @Nonnull
    String getClusterName();

    @Nonnull
    @Info("Can be just a port number (for local connection), or a host:port configuration")
    List<InetSocketAddress> getClusterContactPoints();

    @Default("2000")
    @Info("A global maximum for all queries. Default is 2000")
    int getMaxPageSize();

}
