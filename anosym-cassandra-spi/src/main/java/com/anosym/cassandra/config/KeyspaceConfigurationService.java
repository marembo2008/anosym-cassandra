package com.anosym.cassandra.config;

import java.util.List;

import javax.annotation.Nonnull;

import com.anosym.khameleon.core.annotations.ConfigParam;
import com.anosym.khameleon.core.annotations.Default;
import com.anosym.khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:11:45 AM
 */
public interface KeyspaceConfigurationService {

    @Default("true")
    @Info(""
            + "<ul>"
            + "<li>Optionally (not recommended), bypass the commit log when writing to the keyspace by disabling durable writes (DURABLE_WRITES = false). Default value is true.</li>"
            + "<li>Never disable durable writes when using SimpleStrategy replication<li>"
            + "<ul>")
    boolean isDurableWritesEnabled();

    @Default("1")
    @Info("Assign the same replication factor to the entire cluster. Use for evaluation and single data center test and development environments only")
    int getSimpleStrategyReplicationFactor();

    @Default("1")
    @Info("Assign replication factors to each data center in a comma separated list. Use in production environments and multi-DC test and development environments. Data center names must match the snitch DC name")
    int getDataCentreReplicationFactor(@ConfigParam(name = "dataCentreName") final String dataCentreName);

    @Nonnull
    @Default("SimpleStrategy")
    StrategyClass getStrategyClass();

    @Nonnull
    List<String> getClusters();

}
