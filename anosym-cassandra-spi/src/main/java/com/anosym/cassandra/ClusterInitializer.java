package com.anosym.cassandra;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.anosym.cassandra.config.ClusterConfigurationService;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Configuration;
import com.datastax.driver.core.EndPoint;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.QueryOptions;
import com.google.common.collect.ImmutableList;
import static java.util.stream.Collectors.toList;
import lombok.NonNull;
import sun.rmi.transport.Endpoint;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 12:23:31 PM
 */
@ApplicationScoped
public class ClusterInitializer implements Cluster.Initializer {

  @NonNull
  private final ClusterConfigurationService clusterConfigurationService;

  @Inject
  public ClusterInitializer(@NonNull final ClusterConfigurationService clusterConfigurationService) {
    this.clusterConfigurationService = clusterConfigurationService;
  }

  @Override
  public String getClusterName() {
    return clusterConfigurationService.getClusterName();
  }

  @Override
  public List<EndPoint> getContactPoints() {
    final List<EndPoint> endpoints = clusterConfigurationService.getClusterContactPoints()
            .stream()
            .map((address) -> (EndPoint) (() -> address))
            .collect(toList());
    return endpoints;
  }

  @Override
  public Configuration getConfiguration() {
    final QueryOptions queryOptions = new QueryOptions()
            .setFetchSize(clusterConfigurationService.getMaxPageSize());
    return Configuration.builder()
            .withQueryOptions(queryOptions)
            .build(); //default
  }

  @Override
  public Collection<Host.StateListener> getInitialListeners() {
    //Will be configured for monitoring within an application metrics.
    return ImmutableList.of();
  }

}
