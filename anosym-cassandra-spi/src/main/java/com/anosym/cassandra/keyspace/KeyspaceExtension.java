package com.anosym.cassandra.keyspace;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.Extension;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 11:27:56 PM
 */
public class KeyspaceExtension implements Extension {

    public void registerContext(@Observes final AfterBeanDiscovery afterBeanDiscovery) {
        afterBeanDiscovery.addContext(new KeyspaceScopedContext());
    }

}
