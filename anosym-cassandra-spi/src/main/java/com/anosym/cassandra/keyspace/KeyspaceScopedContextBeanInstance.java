package com.anosym.cassandra.keyspace;

import javax.annotation.Nonnull;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 2, 2018, 5:03:00 AM
 */
@Data
@EqualsAndHashCode(exclude = "context")
class KeyspaceScopedContextBeanInstance<T> {

    @Nonnull
    private final T instance;

    @Nonnull
    private final Bean<T> beanInstance;

    @Nonnull
    private final CreationalContext<T> context;

}
