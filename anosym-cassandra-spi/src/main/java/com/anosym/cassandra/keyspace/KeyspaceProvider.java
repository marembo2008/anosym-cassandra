package com.anosym.cassandra.keyspace;

import javax.annotation.Nullable;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 11:20:12 PM
 */
public interface KeyspaceProvider {

    @Nullable
    String getKeyspace();

}
