package com.anosym.cassandra.keyspace;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalNotification;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.lang.System.getProperty;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 2, 2018, 5:09:50 AM
 */
@Slf4j
enum KeyspaceScopedContextHolder {

    INSTANCE;

    private static final String MAXIMUM_KEYSPACE_COUNTS_KEY = "keyspace.counts";

    private static final String MAXIMUM_KEYSPACE_IDLE_TIME_KEY = "keyspace.idleTime";

    private final int maximumKeyspaceCount;

    private final int maxmumKeyspaceIdleTime;

    private final Cache<BeanKey, KeyspaceScopedContextBeanInstance> keyspacedBeanCache;

    private final Map<BeanKey, Lock> createLocks = new HashMap<>();

    private KeyspaceScopedContextHolder() {
        maximumKeyspaceCount = Integer.parseInt(getProperty(MAXIMUM_KEYSPACE_COUNTS_KEY, "1000"));
        maxmumKeyspaceIdleTime = Integer.parseInt(getProperty(MAXIMUM_KEYSPACE_IDLE_TIME_KEY, "10"));
        keyspacedBeanCache = CacheBuilder
                .<BeanKey, KeyspaceScopedContextBeanInstance>newBuilder()
                .maximumSize(maximumKeyspaceCount)
                .expireAfterAccess(maxmumKeyspaceIdleTime, TimeUnit.MINUTES)
                .concurrencyLevel(Runtime.getRuntime().availableProcessors() * 2)
                .removalListener(this::onRemoval)
                .build();
    }

    @Nullable
    public <T> T getOrCreate(@NonNull final String keyspace,
                             @NonNull final Contextual<T> contextual,
                             @Nullable final CreationalContext<T> context) {
        final Bean<T> bean = (Bean<T>) contextual;
        final BeanKey beanKey = new BeanKey(keyspace, bean.getTypes(), bean.getBeanClass());
        final T beanInstance = getExistingInstance(beanKey);
        if (beanInstance != null) {
            return beanInstance;
        }

        if (context == null) {
            //We cannot create
            return null;
        }

        final Lock createLock = getLock(beanKey);
        createLock.lock();

        //after acquiring lock, be sure someone else did not create it.
        final T contendedBeanInstance = getExistingInstance(beanKey);
        if (contendedBeanInstance != null) {
            return contendedBeanInstance;
        }

        try {
            log.debug("Creating bean for keyspace: {}", keyspace);

            final T newInstance = contextual.create(context);
            if (newInstance == null) {
                throw new IllegalStateException("Unable to create bean: " + contextual);
            }

            final KeyspaceScopedContextBeanInstance<T> newBeanInstance
                    = new KeyspaceScopedContextBeanInstance<>(newInstance, bean, context);
            keyspacedBeanCache.put(beanKey, newBeanInstance);

            return newInstance;
        } finally {
            createLock.unlock();
        }
    }

    @Nullable
    private <T> T getExistingInstance(@NonNull final BeanKey beanKey) {
        final KeyspaceScopedContextBeanInstance<T> beanInstance = keyspacedBeanCache.getIfPresent(beanKey);
        if (beanInstance != null) {
            return beanInstance.getInstance();
        }

        return null;
    }

    <T> void removeAllFor(@NonNull final Contextual<T> contextual) {
        final Class<?> beanClass = ((Bean<T>) contextual).getBeanClass();
        keyspacedBeanCache
                .asMap()
                .keySet()
                .stream()
                .filter((beanKey) -> beanKey.getBeanClass() == beanClass)
                .forEach((beanKey) -> removeBean(beanKey, contextual));
    }

    private <T> void removeBean(@NonNull final BeanKey beanKey, @NonNull final Contextual<T> contextual) {
        final Lock removeLock = getLock(beanKey);
        removeLock.lock();
        try {
            final KeyspaceScopedContextBeanInstance<T> beanInstance
                    = keyspacedBeanCache.getIfPresent(beanKey);
            keyspacedBeanCache.invalidate(beanKey);

            //also call contextual destroy
            contextual.destroy(beanInstance.getInstance(), beanInstance.getContext());
        } finally {
            removeLock.unlock();
        }

    }

    private void onRemoval(@NonNull final RemovalNotification<BeanKey, KeyspaceScopedContextBeanInstance> notification) {
        log.debug("Automatically passivating bean: {}", notification.getKey());

        final KeyspaceScopedContextBeanInstance beanInstance = notification.getValue();
        if (beanInstance == null) {
            return;
        }

        beanInstance.getBeanInstance().destroy(beanInstance.getInstance(), beanInstance.getContext());
    }

    @NonNull
    private Lock getLock(@NonNull final BeanKey beanKey) {
        return createLocks.computeIfAbsent(beanKey, (cls) -> new ReentrantLock());
    }

    @Data
    @EqualsAndHashCode(exclude = "beanClass")
    private static final class BeanKey {

        @Nonnull
        private final String keyspace;

        @Nonnull
        private final Set<Type> beanTypes;

        @NonNull
        private final Class<?> beanClass;

    }

}
