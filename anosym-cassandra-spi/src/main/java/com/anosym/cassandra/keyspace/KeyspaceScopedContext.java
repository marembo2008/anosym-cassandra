package com.anosym.cassandra.keyspace;

import java.lang.annotation.Annotation;

import javax.annotation.Nullable;
import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.context.spi.AlterableContext;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.CDI;

import com.anosym.cassandra.annotation.KeyspaceScoped;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 11:31:00 PM
 */
@Slf4j
public class KeyspaceScopedContext implements AlterableContext {

    private static final KeyspaceScopedContextHolder KEYSPACE_CONTEXT_HOLDER = KeyspaceScopedContextHolder.INSTANCE;

    @Override
    public Class<? extends Annotation> getScope() {
        return KeyspaceScoped.class;
    }

    @Override
    public <T> T get(@NonNull final Contextual<T> contextual, @Nullable final CreationalContext<T> creationalContext) {
        checkContextActive();

        final String keyspace = getKeyspace();

        log.debug("Creating bean <{}>, on keyspace <{}>", ((Bean) contextual).getBeanClass(), keyspace);

        return KEYSPACE_CONTEXT_HOLDER.getOrCreate(keyspace, contextual, creationalContext);
    }

    @Override
    public <T> T get(@NonNull final Contextual<T> contextual) {
        checkContextActive();

        final String keyspace = getKeyspace();

        log.debug("Retrieving bean <{}>, on keyspace <{}>", ((Bean) contextual).getBeanClass(), keyspace);

        return KEYSPACE_CONTEXT_HOLDER.getOrCreate(keyspace, contextual, null);
    }

    @Override
    public boolean isActive() {
        final Instance<KeyspaceProvider> keyspaceProviders = CDI.current().select(KeyspaceProvider.class);
        if (keyspaceProviders.isUnsatisfied()) {
            return false;
        }

        final KeyspaceProvider keyspaceProvider = keyspaceProviders.get();
        final String keyspace = keyspaceProvider.getKeyspace();

        log.debug("Current active keyspace: {}", keyspace);

        return keyspace != null;
    }

    @Override
    public void destroy(@NonNull final Contextual<?> contextual) {
        try {
            KEYSPACE_CONTEXT_HOLDER.removeAllFor(contextual);
        } catch (final Exception ex) {
            log.warn("Error occurred while cleaning keyspace context: {}", contextual, ex);
        }
    }

    @NonNull
    private String getKeyspace() {
        final KeyspaceProvider keyspaceProvider = getKeyspaceProvider();
        final String keyspace = keyspaceProvider.getKeyspace();

        return checkNotNull(keyspace, "No keyspace in current context");
    }

    @NonNull
    private KeyspaceProvider getKeyspaceProvider() {
        return CDI.current().select(KeyspaceProvider.class).get();
    }

    private void checkContextActive() {
        if (!isActive()) {
            throw new ContextNotActiveException("No keyspace is defined for current thread execution");
        }
    }

}
