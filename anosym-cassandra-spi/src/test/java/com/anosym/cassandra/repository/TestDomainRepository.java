package com.anosym.cassandra.repository;

import java.util.UUID;

import com.anosym.cassandra.domain.TestDomain;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 7:53:48 PM
 */
@Accessor
public interface TestDomainRepository {

    @Query("select * from test_domain where id = :id")
    TestDomain findDomain(@NonNull @Param("id") final UUID id);

}
