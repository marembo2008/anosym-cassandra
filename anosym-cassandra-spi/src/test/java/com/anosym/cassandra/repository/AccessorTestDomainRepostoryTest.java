package com.anosym.cassandra.repository;

import java.util.UUID;

import javax.inject.Inject;

import com.anosym.cassandra.domain.TestDomain3;
import com.anosym.tests.cassandra.Cql;
import com.anosym.tests.junit.CdiCassandraJUnitRunner;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 7, 2018, 9:51:01 PM
 */
@Slf4j
@RunWith(CdiCassandraJUnitRunner.class)
@Cql(resourcePath = "/data/cassandra/clean.cql", phase = Cql.ExecutionPhase.AFTER_TEST_METHOD)
@Cql(resourcePath = "/data/cassandra/keyspaces.cql", phase = Cql.ExecutionPhase.BEFORE_TEST_METHOD)
public class AccessorTestDomainRepostoryTest {

    @Inject
    private CrudRepository<TestDomain3, UUID> testDomain3Repository;

    @Inject
    private TestDomain3Accessor testDomain3Accessor;

    @Inject
    private TestKeyspaceProvider keyspaceProvider;

    @Before
    public void setUp() {
        keyspaceProvider.setKeyspace("test2");
    }

    @Test
    public void test_assertIsAnAccessor() {
        assertThat(testDomain3Repository, instanceOf(TestDomain3Accessor.class));
    }

    @Test
    public void test_findById_throughCrudRepository() {
        final UUID id = UUID.fromString("62e6579e-5239-11e8-8df0-9bf11369ce03");
        final TestDomain3 domain3 = testDomain3Repository.find(id);
        final TestDomain3 expectedDomain3 = new TestDomain3();
        expectedDomain3.setId(id);
        expectedDomain3.setType("domain3");
        expectedDomain3.setAmount(5000L);
        assertThat(domain3, equalTo(expectedDomain3));
    }

    @Test
    public void test_findById_throughAccessor() {
        final UUID id = UUID.fromString("62e6579e-5239-11e8-8df0-9bf11369ce03");
        final TestDomain3 domain3 = testDomain3Accessor.getTestDomain(id);
        final TestDomain3 expectedDomain3 = new TestDomain3();
        expectedDomain3.setId(id);
        expectedDomain3.setType("domain3");
        expectedDomain3.setAmount(5000L);
        assertThat(domain3, equalTo(expectedDomain3));
    }

}
