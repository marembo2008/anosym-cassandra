package com.anosym.cassandra.repository;

import javax.enterprise.context.ApplicationScoped;

import com.anosym.cassandra.keyspace.KeyspaceProvider;
import lombok.Setter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 6, 2018, 6:46:39 AM
 */
@ApplicationScoped
public class TestKeyspaceProvider implements KeyspaceProvider {

    @Setter
    private String keyspace = "test";

    @Override
    public String getKeyspace() {
        return keyspace;
    }

}
