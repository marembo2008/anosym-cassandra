package com.anosym.cassandra.repository;

import java.util.UUID;

import javax.inject.Inject;

import com.anosym.cassandra.domain.TestDomain;
import com.anosym.tests.cassandra.Cql;
import com.anosym.tests.junit.CdiCassandraJUnitRunner;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 7:59:03 PM
 */
@Slf4j
@RunWith(CdiCassandraJUnitRunner.class)
@Cql(resourcePath = "/data/cassandra/clean.cql", phase = Cql.ExecutionPhase.AFTER_TEST_METHOD)
@Cql(resourcePath = "/data/cassandra/keyspaces.cql", phase = Cql.ExecutionPhase.BEFORE_TEST_METHOD)
public class TestDomainRepositoryTest {

    @Inject
    private TestDomainRepository testDomainRepository;

    @Inject
    private TestKeyspaceProvider keyspaceProvider;

    @Test
    public void test_loadDomainById0() {
        test(UUID.fromString("332b246c-4e42-11e8-977d-b3aa7d8e2ca2"), "test2");
    }

    @Test
    public void test_loadDomainById1() {
        test(UUID.fromString("a1b9c11e-4d6a-11e8-a628-077fc341df08"), "test");
    }

    private void test(final UUID id, final String keyspace) {
        keyspaceProvider.setKeyspace(keyspace);

        final TestDomain testDomain = testDomainRepository.findDomain(id);
        assertThat(testDomain, hasProperty("id", equalTo(id)));
    }

}
