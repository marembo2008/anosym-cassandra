package com.anosym.cassandra.repository;

import java.util.UUID;

import com.anosym.cassandra.domain.TestDomain3;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 7, 2018, 9:49:03 PM
 */
@Accessor
public interface TestDomain3Accessor extends CrudRepository<TestDomain3, UUID> {

    @Query("select * from test_domain3 where id = :id")
    public TestDomain3 getTestDomain(@Param("id") final UUID id);

}
