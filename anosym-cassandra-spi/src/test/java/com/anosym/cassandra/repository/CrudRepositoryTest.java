package com.anosym.cassandra.repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import javax.inject.Inject;

import com.anosym.cassandra.domain.Page;
import com.anosym.cassandra.domain.TestDomain;
import com.anosym.cassandra.domain.TestDomain2;
import com.anosym.cassandra.domain.TestDomain5;
import com.anosym.tests.cassandra.Cql;
import com.anosym.tests.junit.CdiCassandraJUnitRunner;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.jboss.weld.util.collections.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.stream.Collectors.toList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 7:59:03 PM
 */
@Slf4j
@RunWith(CdiCassandraJUnitRunner.class)
@Cql(resourcePath = "/data/cassandra/clean.cql", phase = Cql.ExecutionPhase.AFTER_TEST_METHOD)
@Cql(resourcePath = "/data/cassandra/keyspaces.cql", phase = Cql.ExecutionPhase.BEFORE_TEST_METHOD)
public class CrudRepositoryTest {

    @Inject
    private CrudRepository<TestDomain, UUID> testDomain1Repository;

    @Inject
    private CrudRepository<TestDomain2, UUID> testDomain2Repository;

    @Inject
    private CrudRepository<TestDomain5, UUID> testDomain5Repository;

    @Inject
    private TestKeyspaceProvider keyspaceProvider;

    @Before
    public void setUp() {
        keyspaceProvider.setKeyspace("test2");
    }

    @Test
    public void test_loadDomainById() {
        final UUID id = UUID.fromString("332b246c-4e42-11e8-977d-b3aa7d8e2ca2");
        final TestDomain testDomain = testDomain1Repository.find(id);
        assertThat(testDomain, hasProperty("id", equalTo(id)));
    }

    @Test
    public void test_saveNewDomain() {
        final UUID key = UUID.fromString("833ab0c2-50e9-11e8-8433-bf1992d61ded");
        final TestDomain testDomain = testDomain1Repository.find(key);
        assertThat(testDomain, is(nullValue()));

        final TestDomain newTestDomain = new TestDomain();
        newTestDomain.setId(key);
        newTestDomain.setName("new Test Domain");
        newTestDomain.setTitle("new test title");
        testDomain1Repository.save(newTestDomain);

        final TestDomain savedNewtestDomain = testDomain1Repository.find(key);
        assertThat(savedNewtestDomain, hasProperty("id", equalTo(key)));
    }

    @Test
    public void test_saveNewDomain2_differentRepository() {
        final UUID key = UUID.fromString("409ab302-5120-11e8-8fea-871756a4b7f7");
        final TestDomain2 testDomain = testDomain2Repository.find(key);
        assertThat(testDomain, is(nullValue()));

        final TestDomain2 newTestDomain = new TestDomain2();
        newTestDomain.setId(key);
        newTestDomain.setIndexType(5002);
        newTestDomain.setValue(23.52);
        testDomain2Repository.save(newTestDomain);

        final TestDomain2 savedNewtestDomain = testDomain2Repository.find(key);
        assertThat(savedNewtestDomain, equalTo(newTestDomain));
    }

    @Test
    public void test_saveAListOfDomains() {
        final List<TestDomain> domains = ImmutableList.of(
                createDomain(UUID.randomUUID()), createDomain(UUID.randomUUID()));
        testDomain1Repository.save(domains);

        domains.forEach((domain) -> {
            final TestDomain savedDomain = testDomain1Repository.find(domain.getId());
            assertThat(savedDomain, equalTo(domain));
        });
    }

    @Test
    public void test_findAListOfDomains() {
        final List<TestDomain5> domains = ImmutableList.of(
                createDomain5(UUID.randomUUID()), createDomain5(UUID.randomUUID()));
        testDomain5Repository.save(domains);

        final List<TestDomain5> savedDomains = testDomain5Repository.findAll();

        log.debug("domain5: {}", savedDomains);

        assertThat(savedDomains, containsInAnyOrder(domains.stream().map(Matchers::equalTo).collect(toList())));
    }

    @Test
    public void test_finalPagedRequests() {
        final List<TestDomain5> allDomains = createDomains(8);
        testDomain5Repository.save(allDomains);

        final int pageSize = 5;
        final Page<TestDomain5> firstPage = testDomain5Repository.findAll(pageSize, null);
        assertThat(firstPage.getContents(), hasSize(5));

        final Page<TestDomain5> secondPage = testDomain5Repository.findAll(pageSize, firstPage.getPagingState());
        assertThat(secondPage.getContents(), hasSize(3));
    }

    @NonNull
    private TestDomain createDomain(@NonNull final UUID id) {
        final TestDomain newTestDomain = new TestDomain();
        newTestDomain.setId(id);
        newTestDomain.setName("new Test Domain-" + id);
        newTestDomain.setTitle("new test title-" + id);
        return newTestDomain;
    }

    @NonNull
    private TestDomain5 createDomain5(@NonNull final UUID id) {
        final TestDomain5 newTestDomain = new TestDomain5();
        newTestDomain.setId(id);
        newTestDomain.setName("Domain-" + id);
        newTestDomain.setTitle("title-" + id);
        return newTestDomain;
    }

    @NonNull
    private List<TestDomain5> createDomains(final int count) {
        return IntStream.range(0, count)
                .mapToObj((i) -> UUID.randomUUID())
                .map(this::createDomain5)
                .collect(toList());
    }

}
