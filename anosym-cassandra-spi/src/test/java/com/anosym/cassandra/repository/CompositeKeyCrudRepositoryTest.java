package com.anosym.cassandra.repository;

import java.util.UUID;

import javax.inject.Inject;

import com.anosym.cassandra.domain.CompositePK;
import com.anosym.cassandra.domain.TestDomain4;
import com.anosym.tests.cassandra.Cql;
import com.anosym.tests.junit.CdiCassandraJUnitRunner;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 7:56:43 AM
 */
@Slf4j
@RunWith(CdiCassandraJUnitRunner.class)
@Cql(resourcePath = "/data/cassandra/clean.cql", phase = Cql.ExecutionPhase.AFTER_TEST_METHOD)
@Cql(resourcePath = "/data/cassandra/keyspaces.cql", phase = Cql.ExecutionPhase.BEFORE_TEST_METHOD)
public class CompositeKeyCrudRepositoryTest {

    @Inject
    private CrudRepository<TestDomain4, CompositePK> testDomain4Repostiry;

    @Inject
    private TestKeyspaceProvider keyspaceProvider;

    @Before
    public void setUp() {
        keyspaceProvider.setKeyspace("test2");
    }

    @Test
    public void testCompositeKey() {
        final UUID key1 = UUID.randomUUID();
        final UUID key2 = UUID.randomUUID();
        final TestDomain4 domain = new TestDomain4();
        domain.setKey1(key1);
        domain.setKey2(key2);
        domain.setType("type1");
        domain.setAmount(6777L);
        testDomain4Repostiry.save(domain);

        final CompositePK lookupKey = new CompositePK();
        lookupKey.setKey1(key1);
        lookupKey.setKey2(key2);

        final TestDomain4 expectedDomain = testDomain4Repostiry.find(lookupKey);
        assertThat(expectedDomain, equalTo(domain));
    }

}
