package com.anosym.cassandra.config;

import java.net.InetSocketAddress;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.weld.util.collections.ImmutableList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:00:03 PM
 */
@ApplicationScoped
public class TestClusterConfigurationService implements ClusterConfigurationService {

    @Override
    public String getClusterName() {
        return "TestCluster";
    }

    @Override
    public List<InetSocketAddress> getClusterContactPoints() {
        return ImmutableList.of(new InetSocketAddress("127.0.0.1", 58000));
    }

    @Override
    public int getMaxPageSize() {
        return 5000;
    }

}
