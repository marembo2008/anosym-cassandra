package com.anosym.cassandra.service;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import com.anosym.cassandra.annotation.CompositeKeyClass;
import com.anosym.cassandra.domain.CompositePK;
import com.anosym.cassandra.repository.CrudRepository;
import com.anosym.cassandra.repository.TestKeyspaceProvider;
import com.anosym.cdi.executionscoped.ExecutionScoped;
import com.anosym.tests.junit.CdiCassandraJUnitRunner;
import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jboss.weld.util.collections.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 8, 2018, 6:40:20 AM
 */
@Slf4j
@RunWith(CdiCassandraJUnitRunner.class)
public class KeyspaceDefinitionServiceTest {

    @Inject
    private KeyspaceDefinitionService keyspaceDefinitionService;

    @Inject
    private CrudRepository<KeyspaceScriptDomain, CompositePK> keyspaceScriptDomainRepository;

    @Inject
    private TestKeyspaceProvider testKeyspaceProvider;

    @Test
    public void testCreateKeyspace() {
        keyspaceDefinitionService.addKeyspace("keyspace_definition");

        final List<Keyspace> keyspaces = keyspaceDefinitionService.getApplicationKeyspaces();
        assertThat(keyspaces, hasItem(new Keyspace("keyspace_definition", true)));
    }

    @Test
    public void testCreateKeyspaceAndLoadSCript() {
        keyspaceDefinitionService.addKeyspace("keyspace_definition2");

        final List<Keyspace> keyspaces = keyspaceDefinitionService.getApplicationKeyspaces();
        assertThat(keyspaces, hasItem(new Keyspace("keyspace_definition2", true)));

        final UUID uuid = UUID.fromString("b828586c-5561-11e8-a10c-cfe09b229f2d");
        final UUID localeUuid = UUID.fromString("c2de5810-5561-11e8-a4fa-c32a0520fc66");
        final CompositePK pk = new CompositePK();
        pk.setKey1(uuid);
        pk.setKey2(localeUuid);

        final KeyspaceScriptDomain expected = new KeyspaceScriptDomain();
        expected.setUuid(uuid);
        expected.setDefaultLocaleUuid(localeUuid);
        expected.setName("Test Context");

        // Set the keyspace
        testKeyspaceProvider.setKeyspace("keyspace_definition2");
        assertThat(keyspaceScriptDomainRepository.find(pk), equalTo(expected));
    }

    @ExecutionScoped
    public static class TestScriptProvider implements ScriptProvider {

        @Override
        public List<String> getScriptPaths(@NonNull final String keyspace) {
            if (keyspace.equals("keyspace_definition2")) {
                return ImmutableList.of("/data/cassandra/test_dml_script.cql");
            }

            return ImmutableList.of();
        }

    }

    @Data
    @Table(name = "translation_contexts")
    @CompositeKeyClass(CompositePK.class)
    public static class KeyspaceScriptDomain {

        @PartitionKey
        private UUID uuid;

        private String name;

        @ClusteringColumn
        private UUID defaultLocaleUuid;

    }

}
