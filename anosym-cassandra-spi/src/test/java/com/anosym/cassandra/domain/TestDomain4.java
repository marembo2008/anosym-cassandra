package com.anosym.cassandra.domain;

import java.util.UUID;

import com.anosym.cassandra.annotation.CompositeKeyClass;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 7:52:14 PM
 */
@Data
@Table(name = "test_domain4")
@CompositeKeyClass(CompositePK.class)
public class TestDomain4 {

    @PartitionKey
    private UUID key1;

    @PartitionKey(1)
    private UUID key2;

    private String type;

    private long amount;

}
