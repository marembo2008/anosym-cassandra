package com.anosym.cassandra.domain;

import java.util.UUID;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 7:52:14 PM
 */
@Data
@Table(name = "test_domain")
public class TestDomain {

    @PartitionKey
    private UUID id;

    private String name;

    private String title;

}
