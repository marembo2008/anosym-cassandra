package com.anosym.cassandra.domain;

import java.util.UUID;

import com.anosym.cassandra.annotation.CompositeKey;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 9, 2018, 7:53:19 AM
 */
@Data
@CompositeKey
public class CompositePK {

    private UUID key1;

    private UUID key2;

}
